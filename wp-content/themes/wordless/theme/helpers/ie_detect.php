<?php

/*
 * Check the header file for IE
 */

class ifie {

  public static function _string () {
    $ie = (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT'])) ? 'ie' : 'no-ie';
    return $ie;
  }
  
  public static function _boole () {
    return (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']));
  }
  
}

?>