<?php

/**
 * Hide ACF menu item from the admin menu
 */
 
function hide_admin_menu() {
	global $current_user;
	get_currentuserinfo();
 
	if ($current_user->user_login != 'admin') {
		echo '<style type="text/css">#toplevel_page_edit-post_type-acf{display:none;}</style>';
	}
}
 
add_action('admin_head', 'hide_admin_menu');

?>