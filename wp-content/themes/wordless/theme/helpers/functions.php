<?php

global $wpdb;

// Simply makes a properly formated i tag for making icons

function boot_icon( $icon, $white = false ) {
  $class = ( $white ) ? $icon . ' icon-white' : $icon;
  $tag = "<i class=\"$class\"></i>";
  return $tag;
}

// Returns the category ID given a category name or slug

function get_category_id($cat_name){
	$term = get_term_by('name', $cat_name, 'category');
	return $term->term_id;
}

/*
 * Helper functions to make mailtos easier
 * add this to the hred attribute of an a tag
 */
 
function mailto( $email, $subject = '', $message = '' ) {
  return "mailto:$email?subject=$subject&body=$message";
}


// Make easy as pie simple bootstrap form fields

function form_text_field( $label = '', $name = '', $placeholder = '' ) {
  $html = <<<html
  <div class="control-group">
    <label class="control-label" for="$name">$label</label>
    <div class="controls">
      <input id="$name" name="$name" type="text" placeholder="$placeholder" />
    </div>
  </div>
html;

  return $html;
}

function form_radio_field( $label = '', $name = '', $options = array() ) {
  $open = <<<open
  <div class="control-group">
    <label class="control-label" for="$name">$label</label>
    <div class="controls">
open;

  foreach( $options as $radio ) {
    $tag = <<<tag
    <label class="radio inline">
      <input type="radio" name="$name" value="$radio[value]" />
      $radio[label]
    </label>
tag;

    $open = $open . $tag;
  }
  
  $close = <<<close
    </div>
  </div>
close;

  $html = $open . $close;
  return $html;
}

/* ======================================================
 * 
 * @SECTION:  Form Data Handling Functions
 * @AUTHOR:   Adam
 * @FUNCITON: Used to 'easily' manage forms submitted
 *            using AJAX on the front end
 *
 * =================================================== */ 

/*
 * Appends a properly formated flash alert to the JSON return array
 * $a => the JSON array
 * $success, $warning, $failure => Strings to be added to the flash system
 */

function new_flash( $success = 0, $warning = 0, $failure = 0 ) {
  $a = array();
  $a['success'] = $success;
  $a['warning'] = $warning;
  $a['error']   = $failure;
  return $a;
}

/*
 * Basic data collection for all forms
 * creates an array to be submitted to database
 * also a basis for JSON returns
 */
 
function form_data() {
  global $wpdb;
  $data = $_POST;
  unset( $data["_target"] );
  $data['time_submitted'] = date( 'c' );
  $data['user_agent']     = $_SERVER['HTTP_USER_AGENT'];
  $data['ip']             = $_SERVER['REMOTE_ADDR'];
  $data['host']           = $_SERVER['REMOTE_HOST'];
  return $data;
}

function insert_into_db( $data, $table ) {
  global $wpdb;
  $table_name = $wpdb->prefix . $table;
  $wpdb->insert( $table_name, $data );
  return $data;
}

function json_return( $data, $flash = array(), $a = NULL ) {
  global $wpdb;
  $return_value = array();
  $return_value['id']   = $wpdb->insert_id;
  $return_value['data'] = $data;
  
  $return_value['flash'] = $flash;
  
  if ( $a )
    $return_value['info'] = $a;
  
  echo json_encode( $return_value );
  return $data;
}

/*
 * Takes a file uploaded through a form and saves it to the 
 * wp_uploads directory 
 */

function capture_uploaded_file( $form_name, $dir = '/files/'  ) {
  global $wpdb;

  // Where the file is going to be placed 
  $uploads = wp_upload_dir();
  $uploads_dir = $uploads["basedir"] . $dir;
  
  $file = $_FILES[$form_name];
  $name = preg_replace("/[^A-Z0-9._-]/i", "_", $file["name"]);
  
  // Ensure we don't overwrite any files
  $i = 0;
  $parts = pathinfo($name);
  while (file_exists( $uploads_dir . $name)) {
    $i++;
    $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
  }
  
  // Move file into place and set permissions
  $target_path = $uploads_dir . basename( $name );
  if(move_uploaded_file($file['tmp_name'], $target_path)) {
      $path_to_file = 'http://' . $_SERVER["HTTP_HOST"] . $uploads["baseurl"] . $dir . basename( $name );
      chmod( $uploads_dir . $name, 0644);
  } else{
      $path_to_file = false;
  }
  
  return array( $name, $path_to_file );
}

/*
 * All these functions may be used seperatly, but 
 * for basic forms with nothing special just run this
 */ 

function basic_form( $table, $passed, $failed ) {
  global $wpdb;
  $DATA = insert_into_db( form_data(), $table );
  $flash = new_flash();
  if ( $wpdb->insert_id ) {
    $flash['success'] = $passed;
  } else {
    $flash['error'] = $failed;
    $flash['warning'] = $wpdb->last_error;
  }
  json_return( $DATA, $flash );
}