<?php

function cpp_file_print () {
  $file = get_field('cpp_file');
  $title = get_field('cpp_title');
  $markup = link_to( $title, $file );
  
  return "<div class='file'>$markup</div>";
}

add_shortcode('cpp_file', 'cpp_file_print');

?>