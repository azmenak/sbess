<?php

/*
 * Helped classes to make accesing the 
 * mobile detect library easier for 
 * our purposes
 */

require_once("libs/Mobile_Detect.php");
global $detect;
$detect = new Mobile_Detect();

class ifmobile {

  public static function test () {
    global $detect;
    $test = ($detect->isMobile()) ? 'mobile' : 'desktop';
    return $test;
  }
  
  public static function mobile () {
    global $detect;
    $mob = ($detect->isMobile()) ? 'mobile' : '';
    return $mob;
  }
  
  public static function tablet () {
    global $detect;
    $tab = ($detect->isTablet()) ? 'tablet' : '';
    return $mob;
  }
  
}

?>