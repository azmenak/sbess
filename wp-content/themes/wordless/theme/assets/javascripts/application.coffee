# =================================
# This main application file which gets loaded across all pages
# use the templates coffee files for individual template scripts
# =================================

$ ->


  # =================================
  # We are using a conditional loader on DOM ready to load content 
  # libraries, this provides a more centralized script control
  # =================================
  
  Modernizr.load [
    {    
      test : !Modernizr.input.placeholder && $('[placeholder]').length
      yep : 
        placeholder : window.jspath + 'libs/jquery.placeholder.min.js'
      complete : (url, result, key) ->
        if result
          $( 'input, textarea' ).placeholder()
          console.log 'placeholder loaded'
        [url, result, key]
    }
    {
      test : !!$('.fittext').length
      yep :
        fittext : window.jspath + 'jquery.fittext.min.js'
      callback : (url, result, key) ->
        if result
          $('.fittext').fittext()
          console.log 'fittext loaded'
        [url, result, key]
    }
    {
      test : typeof window.carousel_interval != 'undefined'
      yep : 
        carousel : window.jspath + 'bootstrap/bootstrap-carousel.js'
      callback : (url, result, key) ->
        if result
          $('.carousel').carousel
            interval : window.carousel_interval
            console.log 'carousel loaded'
        [url, result, key]
    }
    {
      test : !!$('.fancybox').length
      yep : 
        fbjs   : window.jspath + 'fancybox/jquery.fancybox.pack.js'
        fbcss  : window.jspath + 'fancybox/jquery.fancybox.css'
        btnjs  : window.jspath + 'fancybox/helpers/jquery.fancybox-buttons.js'
        btncss : window.jspath + 'fancybox/helpers/jquery.fancybox-buttons.css'
        tmbjs  : window.jspath + 'fancybox/helpers/jquery.fancybox-thumbs.js'
        tmbcss : window.jspath + 'fancybox/helpers/jquery.fancybox-thumbs.css'
      complete : (url, result, key) ->
        if !!$('.fancybox').length
          $('.fancybox').fancybox
            openEffect : 'elastic'
            closeEffect : 'elastic'
            prevEffect : 'fade'
            nextEffect : 'fade'
            helpers :
              title :
                type : 'outside'
              buttons : {}
              thumbs :
                width : 100
                height : 100
        [url, result, key]
    }
  ]
  
  # ==================================
  # Get ready for this, we are creating a modal out 
  # of nothing!
  # ==================================
  
  $('#modal').dialog
    autoOpen : false
    zIndex : 8000
    draggable : true
  
    buttons :
      close :
        text : 'Close'
        class : 'btn'
        click : ->
          $(this).dialog "close"
      send :
        text : 'Send'
        class : 'btn btn-primary'
        click : ->
          $(this).dialog "close"
        
    width : 500
    height : 'auto'
    position : [ 'center', $(window).height() * 0.2 ]
    
    minWidth : 430
    minHeight : 200
    
    show :
      effect : 'drop'
      direction : 'up'
    hide :
      effect : 'drop'
      direction : 'up'
    
  # ================================
  # Login controller section
  # ================================  
  
  $('#login').on 'click', (e) ->  
  
    e.preventDefault()
    
    $modal = $('#modal')
    $modal.dialog
      title : 'SBESS Login'
      buttons :
        close :
          text : 'Close'
          priority : 'secondary'
          class : 'btn'
          click : ->
            $(this).dialog 'close'
        login :
          text : 'Login'
          priority : 'primary'
          class : 'btn btn-primary'
          click : ->
    
    $content = $('#forms > .login-form').children().clone()
    $modal.html( $content )
    $modal.dialog 'open'
    
  # ==============================
  # Navbar scrolling controller
  # ==============================
  
  breakpoint = 190
  aboveFold = ( $(window).scrollTop() <= breakpoint ) ? true : false
  $nav = $('header nav')
  
  if aboveFold then $nav.addClass( 'above' )
  else $nav.addClass( 'below' )
  
  $(window).scroll ->
    if not MOBILE
      if aboveFold and $(window).scrollTop() > breakpoint
        aboveFold = false
        $nav.toggleClass( 'above below' )
      if !aboveFold and $(window).scrollTop() <= breakpoint
        aboveFold = true
        $nav.toggleClass( 'above below' )
        
  # =============================
  # Controller for the hiring form
  # =============================
  
  $rt = $(' .round-table > form' )
  $rt.on 'submit', (e) ->
    e.preventDefault()
    
    # Empty fields  
    valid = true
    $rt.find( 'input[type=text]' ).each ->
      if not $(this).val().match(/\S/)
        if valid then valid = false
        $(this).closest( '.control-group' ).addClass( 'error' )
      else
        $(this).closest( '.control-group' ).removeClass( 'error' )
        
    # Must be a PDF file
    _file = $rt.find( 'input[type=file]' )
    if not _file.val().match(/\.pdf$/i)
      _file.closest( '.control-group' )
        .addClass( 'error' )
        .find( '.alert-error' ).slideDown( 300 )
      if valid then valid = false
    else
      _file.closest( '.control-group' )
        .removeClass( 'error' )
        .find( '.alert-error' ).slideUp( 300 )
    
    _message =  $rt.find( '#message' )
    if not valid
      _message.slideDown( 300 )
      return false
    else
      _message.slideUp( 300 )
      
    $rt.find( 'button[type=submit]' ).button('loading')
    $rt.ajaxSubmit ( k ) ->
      data = $.parseJSON( k )
      window.flash = data.flash
      
      _error = ( msg ) ->
        $rt.find( 'button[type=submit]' ).button( true ).html( 'Submit Application' )
        $rt.find( '#error' ).slideDown(300).html( msg )
        $rt.find( '#success' ).slideUp(300)
        $form.find( 'input, textarea, select' ).attr
          disabled : 'false'
          
      _success = ( msg ) ->
        $rt.find( 'button[type=submit]' ).slideUp(300)
        $rt.find( '#error' ).slideUp(300)
        $rt.find( '#success' ).slideDown(300).html( msg )
        $form.find( 'input, textarea, select' ).attr
          disabled : 'disabled'
      
      if window.flash.success then _success( window.flash.success )
      else _error( window.flash.error )
    
  
  $form = $( '.application > form' )
  $form.on 'submit', (e) ->
    e.preventDefault()
    
    # Empty fields  
    valid = true
    $( '.application > form input[type=text], .application > form textarea' ).each ->
      if not $(this).val().match(/\S/)
        if valid then valid = false
        $(this).closest( '.control-group' ).addClass( 'error' )
      else
        $(this).closest( '.control-group' ).removeClass( 'error' )
        
    # Must be a PDF file
    if not $( '.application > form input[type=file]' ).val().match(/.*pdf$/i)
      $( '.application > form input[type=file]' ).closest( '.control-group' )
        .addClass( 'error' )
        .find( '.alert-error' ).slideDown( 300 )
      if valid then valid = false
    else
      $( '.application > form input[type=file]' ).closest( '.control-group' )
        .removeClass( 'error' )
        .find( '.alert-error' ).slideUp( 300 )
        
      
    # At least one job selected
    fails = 0    
    $( '.application [name*=job]' ).each ->
      if not $(this).val().match(/\S/)
        fails++
    if fails > 2
      $( '.application [name*=job]' ).closest( '.control-group')
        .addClass( 'error' )
        .find( '.alert-error' ).slideDown( 300 )
      if valid then valid = false
    else
      $( '.application [name*=job]' ).closest( '.control-group')
        .removeClass( 'error' )
        .find( '.alert-error' ).slideUp( 300 )
    
    # Must be a PDF file
    if not $( '.application > form input[type=file]' ).val().match(/.pdf|.PDF$/)
      $( '.application > form input[type=file]' ).closest( '.control-group' )
        .addClass( 'error' )
        .find( '.alert-error' ).slideDown( 300 )
      if valid then valid = false
    else
      $( '.application > form input[type=file]' ).closest( '.control-group' )
        .removeClass( 'error' )
        .find( '.alert-error' ).slideUp( 300 )
        
    if not valid
      $( '.application > form #message' ).slideDown( 300 )
      return false
    else
      $( '.application > form #message' ).slideUp( 300 )
  
    $form.find( 'button[type=submit]' ).button('loading')
    $form.ajaxSubmit ( k ) ->
    
      data = $.parseJSON( k )

      console.log data
      
      if data.id      
        $form.find( 'button[type=submit]' ).slideUp(300)
        $( '.application > form #error' ).slideUp(300)
        $( '.application > form #success' ).slideDown(300)
        $form.find( 'input, textarea, select' ).attr
          disabled : 'disabled'
          
      else
        $( '.application > form #error' )
          .slideDown(300)
          .html( "Submission not received" )
          
          $('#modal').dialog
            title : 'Application Error'
            buttons :
              close :
                text : 'Close'
                priority : 'secondary'
                class : 'btn'
                click : ->
                  $(this).dialog 'close'
                
            position : [ 'center', $(window).height() * 0.2 ]
            
            open : ->
              $( this ).html( data.message )
              
  
  $contact = $( '.contact > form' )
  $contact.on 'submit', (e) ->
  
    e.preventDefault()
    
    valid = true
    $( '.contact > form input[type=text], .contact > form textarea' ).each ->
      if not $(this).val().match(/\S/)
        if valid then valid = false
        $(this).closest( '.control-group' ).addClass( 'error' )
      else
        $(this).closest( '.control-group' ).removeClass( 'error' )
    
    if not valid
      $( '.contact > form #message' ).slideDown( 300 )
      return false

    $( '.contact > form #message' ).slideUp( 300 )
    
    $contact.find( 'button[type=submit]' ).attr
      disabled : 'disabled'
              
    $contact.ajaxSubmit ( k ) ->

      data = $.parseJSON( k )
      
      if data.id
        $contact.slideUp(300)
        $('.contact #message').slideDown(300)
        
  $feedback = $( '.feedback > form' )
  $feedback.on 'submit', (e) ->
  
    e.preventDefault()
    
    $feedback.find( 'button[type=submit]' ).button( 'loading' )
              
    $feedback.ajaxSubmit ( k ) ->

      data = $.parseJSON( k )
      
      if data.id
        $feedback.find( 'button[type=submit]' ).slideUp(300)
        $('.feedback #message').slideDown(300)
        
  # ================================
  # Generate a compatibility alert 
  # when using IE
  # ================================
  
  if ( typeof ie != 'undefined' ) and not MOBILE
    $modal = $('#modal')
    $modal.dialog
      title : 'Internet Explorer Warning'
      buttons :
        close :
          text : 'OK, I understand'
          class : 'btn'
          click : ->
            $(this).dialog 'close'
        chrome :
          text : 'Get Chrome Frame'
          class : 'btn btn-primary'
          click : ->
            window.location.href = 'http://www.google.com/chromeframe'
            
    $content = $('#forms > .ie-warning').children().clone()
    $modal.html( $content )
    $modal.dialog 'open'

  # =================================
  # Control scrollTo
  # Iterates through each element and adds the controller
  # =================================

  $('.nav-list a[href*=#]').each ()->
    $(this).on 'click', (e)->
      e.preventDefault()
      $.scrollTo $(this).attr('href'), 400

  # =================================
  # Start the async twitter loader
  # make sure you keep this at the bottom so the rest of the scripts 
  # execute before this
  # =================================

  return # end of jQuery wrapper
