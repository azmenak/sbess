# ===================================
# This file serves as a conditional loader for all the scripts needed
# yepnope will load all the assets asynchronously, so there won't be a lag
# while the page loads
# ===================================

# helper function to load conditionally based on classes
# handy for mobile

hasClass = (element, cls) ->
  r = new RegExp('\\b' + cls + '\\b')
  r.test(element.className)

window.jspath = THEMEPATH + '/assets/javascripts/'
window.csspath = THEMEPATH + '/assets/stylesheets/'

dev =
  load :
    jstwitter       : window.jspath + 'twitter.js'
    boot_transition : window.jspath + 'bootstrap/bootstrap-transition.js'
    boot_collapse   : window.jspath + 'bootstrap/bootstrap-collapse.js'
    boot_dropdown   : window.jspath + 'bootstrap/bootstrap-dropdown.js'
    boot_buttons    : window.jspath + 'bootstrap/bootstrap-button.js'
    jquery_ui       : window.jspath + 'jquery-ui.min.js'
    jquery_form     : window.jspath + 'jquery-form.js'
    cookies         : window.jspath + 'utilities/cookies.js?version=1.0'
    scroll_to       : window.jspath + 'jquery.scrollTo-1.4.3.1.js'

Modernizr.load [
  { load : window.jspath + 'jquery.min.js' }
  { load : window.jspath + 'library.min.js' }
  { load : window.jspath + 'application.js' }
]
