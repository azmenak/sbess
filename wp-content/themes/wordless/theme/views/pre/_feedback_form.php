<?php

$data = $_POST;
unset( $data["_target"] );
$data['time_submitted'] = date( 'c' );
$data['user_agent']     = $_SERVER['HTTP_USER_AGENT'];

global $wpdb;
$table_name = $wpdb->prefix . 'feedback';
$wpdb->insert( $table_name, $data );

/*
 * We are going to create an array with the returns values
 * and reuturn is as a json object for use in javascript
 */

$return_value = array();

$return_value['id']   = $wpdb->insert_id;
$return_value['data'] = $data;

echo json_encode( $return_value );
