<?php

global $wpdb;

// Where the file is going to be placed 
$uploads = wp_upload_dir();
$uploads_dir = $uploads["basedir"] . '/round-table/2012/';

$resume = $_FILES["resume_file"];
$name = preg_replace("/[^A-Z0-9._-]/i", "_", $resume["name"]);

// Ensure we don't overwrite any files
$i = 0;
$parts = pathinfo($name);
while (file_exists( $uploads_dir . $name)) {
  $i++;
  $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
}

$target_path = $uploads_dir . basename( $name );

if(move_uploaded_file($resume['tmp_name'], $target_path)) {
    $path_to_resume = 'http://' . $_SERVER["HTTP_HOST"] . $uploads["baseurl"] . '/resumes/' . basename( $name );
    chmod( $uploads_dir . $name, 0644);
} else{
    $path_to_resume = false;
}

$data = form_data();

$data['resume']         = $name;
$data['path_to_resume'] = $path_to_resume;

insert_into_db( $data, 'round_table' );

$flash = new_flash();
$info = array();

if ( !$path_to_resume ) {
  $flash['error'] = 'Could not upload resume';
  if ( !($wpdb->insert_id) ) {
    $flash['error'] = 'Could not upload resume and there was an error inserting into the database';
    $info['mysql_err'] = $wpdb->last_error;
  }
} else if ( !($wpdb->insert_id) ) {
  $flash['error'] = 'There was an error inserting into the database';
  $info['mysql_err'] = $wpdb->last_error;
} else {
  $flash['success'] = 'Resume and data received successfully';
}

json_return( $data, $flash, $info );