<?php

/*
 * First move the resume file into place
 */

// Where the file is going to be placed 

$open = true;

if ( $open ) {

$uploads = wp_upload_dir();
$uploads_dir = $uploads["basedir"] . '/resumes/';

$resume = $_FILES["resume_file"];
$name = preg_replace("/[^A-Z0-9._-]/i", "_", $resume["name"]);

// Ensure we don't overwrite any files
$i = 0;
$parts = pathinfo($name);
while (file_exists( $uploads_dir . $name)) {
  $i++;
  $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
}

$target_path = $uploads_dir . basename( $name );

if(move_uploaded_file($resume['tmp_name'], $target_path)) {
    $path_to_resume = 'http://' . $_SERVER["HTTP_HOST"] . $uploads["baseurl"] . '/resumes/' . basename( $name );
    chmod( $uploads_dir . $name, 0644);
} else{
    $path_to_resume = "There was an error uploading the resume file!";
}

/*
 * Then deal with the mail stuff
 * it will send a really ugly email to each person in the emails array
 */

$emails = array();
$emails[] = 'admin@sbess.ca';


// $emails[] = 'hr@sbess.ca';

$subject = "$_POST[first_name] $_POST[last_name] for $_POST[job1]";

$body = <<<MSG
:: Basic Info ::

NAME: $_POST[first_name] $_POST[last_name]
PROGRAM: $_POST[program]
YEAR: $_POST[school_year]
CCOP: $_POST[coop]
EMAIL: $_POST[mylaurier]@mylaurier.ca

JOB 1: $_POST[job1]
JOB 2: $_POST[job2]
JOB 3: $_POST[job3]

:: Answers to Questions ::

QUESTION 1
$_POST[q1]

QUESTION 2
$_POST[q2]

QUESTION 3
$_POST[q3]

QUESTION 4
$_POST[q4]

LINK TO RESUME: $path_to_resume 
MSG;

$headers = "From: SBESS Application <application.form@sbess.ca>";

mail( implode( ', ', $emails ), $subject, $body, $headers );
mail( 'apps@sbess.ca', $subject, $body, $headers );

}

/*
 * And the fun part
 * add an entry into the database
 */
 
$data = $_POST;
unset( $data["_target"]);

$data["coop"] = ( $data["coop"] ) ? 1 : 0;

$data['time_submitted'] = date( 'c' );
$data['resume']         = $name;
$data['mail_message']   = $body;
$data['path_to_resume'] = $path_to_resume;
$data['user_agent']     = $_SERVER['HTTP_USER_AGENT'];

$return_value = array();
 
if ( $open ) {

global $wpdb;

$table_name = $wpdb->prefix . 'hiring';
$wpdb->insert( $table_name, $data );

$return_value['message'] = $body;
$return_value['resume']  = $path_to_resume;
$return_value['id']      = $wpdb->insert_id;
$return_value['data']    = $data;

$return_value['error'] = array(
  'num' => mysql_errno(),
  'err' => mysql_error()
);

if ( $wpdb->insert_id ) {

$date = date( 'r' );

$confirm = <<<MESSAGE
$_POST[first_name], thank you for applying to be part of the SBESS!

This message is a confirmation that we have received your application and will be reviewing it shortly. You will be contacted about the results of your application within a week after the application deadline. 

For reference,
Application received at: $date
Application ID: $wpdb->insert_id

Cheers and good luck, 

SBESS Management
MESSAGE;

mail( $_POST['mylaurier'].'@mylaurier.ca', 'SBESS Application Confirmation', $confirm, "From: SBESS <applications@sbess.ca>" );

}

} else {

  $return_value['id'] = false;
  $return_value['message'] = 'Application this hiring round are now closed, we are no longer accepting any applications';

}

/*
 * We are going to create an array with the returns values
 * and reuturn is as a json object for use in javascript
 */



echo json_encode( $return_value );