<?php

/*
 * Delegates which form submission to use
 * adding this as a a head to the document allows us to easily use the WP environment when using forms
 */
 
$partial = 'pre/' . $_POST['_target'];
render_partial( $partial );
