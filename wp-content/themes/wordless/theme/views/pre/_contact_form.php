<?php

$data = $_POST;
unset( $data["_target"] );
$data['time_submitted'] = date( 'c' );
$data['user_agent']     = $_SERVER['HTTP_USER_AGENT'];

$passed = true;

global $wpdb;

if ( !preg_match('/[a-zA-Z]{4}[\d]{4}/', $data['mylaurier']) ) {
  $passed = false;
  $message = "Invalid Email: you must use your Novel ID (the first 8 digits of your mylaurier email). Value received: $data[mylaurier]";
} elseif ( preg_match('/[^a-z ,\'.\d]/'), $data['full_name'] ) {
  $passed = false;
  $message = "Sorry, names cannot contain any special characters to help prevent spambots. Value received: $data[full_name]";
}

if ($passed) {
  $table_name = $wpdb->prefix . 'contact';
  $wpdb->insert( $table_name, $data );

  $message = "Your message has been successfully received. We'll be getting back to you as soon as we can.";
}

/*
 * We are going to create an array with the returns values
 * and reuturn is as a json object for use in javascript
 */

$return_value = array();

$return_value['id']     = $wpdb->insert_id;
$return_value['data']   = $data;
$return_value['passed'] = $passed;

echo json_encode( $return_value );
