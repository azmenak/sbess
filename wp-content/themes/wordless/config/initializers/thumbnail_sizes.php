<?php

function add_thumbnails_support() {
  /*
   * Enable WordPress post thumbnail uncommenting the line below and specifing the post types
   */

  add_theme_support('post-thumbnails', array('page', 'post'));
}

function setup_thumbnail_sizes() {
  /*
   * Add additional thumbnail sizes here
   */

  add_image_size("icon", 32, 32, true);
  add_image_size("icon-large", 64, 64, true);

  add_image_size("logo", 160, 160, false);
  
  // Restrict images to 100px height
  add_image_size("small", 9999, 100, false);
  add_image_size("tiny", 9999, 40, false);
  
  // Banner Image
  add_image_size("banner", 1170, 390, true);
  add_image_size("big", 1024, 1024, false);
  
}

add_action('after_setup_theme', 'add_thumbnails_support');
add_action('after_setup_theme', 'setup_thumbnail_sizes');



