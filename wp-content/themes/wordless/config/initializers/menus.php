<?php

/*
 * Enable WordPress menu support uncommenting the line below
 */
add_theme_support('menus');

function register_custom_menus() {
  /*
   * Place here all your register_nav_menu() calls.
   */

  register_nav_menus(
    array( 
      'header-menu' => __( 'Header Menu'),
      'footer-menu' => __( 'Footer Menu')
    )
  );
}

add_action('init', 'register_custom_menus');

function has_sub ($menu_item_id, &$items) {

  foreach ($items as $item) {
    if ($item->menu_item_parent && $item->menu_item_parent==$menu_item_id) {
      return true;
    }
  }
  return false;
};

function add_bootstrap_classes ($items) {
  foreach ($items as &$item) {
    if ( has_sub($item->ID, &$items) ) {
      $item->classes[] = 'menu-parent-item';
      $item->classes[] = 'dropdown';
    }
  }
  return $items;  
}

add_filter('wp_nav_menu_objects', 'add_bootstrap_classes');

function bootstrap_filter ($items) {
  $items = str_replace('current_page_item', 'current_page_item active', $items);
  $items = str_replace('children', 'children nav nav-list', $items);
  
  return $items;
}

add_filter('wp_list_pages', 'bootstrap_filter');

class Bootstrap_Walker extends Walker_Nav_Menu {

  // add classes to ul sub-menus
function start_lvl( &$output, $depth ) {
    // depth dependent classes
    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
    $classes = array(
        'sub-menu',
        'dropdown-menu',
        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
        ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
        'menu-depth-' . $display_depth
        );
    $class_names = implode( ' ', $classes );
  
    // build html
    $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
}
  
// add main/sub classes to li's and links
 function start_el( &$output, $item, $depth, $args ) {
    global $wp_query;
    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
  
    // depth dependent classes
    $depth_classes = array(
        ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
        ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
        ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
        'menu-item-depth-' . $depth
    );
    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
  
    // passed classes
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
  
    // build html
    $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
  
    // link attributes
    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
    $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link dropdown-toggle' ) . '"';
    $attributes .= ( $depth > 0 ? '' : 'data-toggle="dropdown"' );
  
    $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
        $args->before,
        $attributes,
        $args->link_before,
        apply_filters( 'the_title', $item->title, $item->ID ),
        $args->link_after,
        $args->after
    );
  
    // build html
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
}
  
}

