SBESS User Facing Site
======================

* This file was written in iA writer for Mac ( a fantastic application ) and might look slightly off in other apps

* DO NOT UPGRADE THE WORDPRESS INSTALL
	- some of the files in the wordpress core have been modified for the site, upgrading will break some functionality ( like the menus )

## Directives

- footer
	- contact form 完成
	- twitter feed

- office booking
- SBE weekly submissions
	- 30 chars for title
	- unlimited story
	- optional picture ( max 100kb )
- CR
- O-Day
- photos


## Separate Pages
- Balls
- ODay
- SBE Cares
- JDC 完成
- LACC
- 5Days

## Decisions we need to make
- Do we include eventbrite or something like that for the events
	- We don't, it's way too expensive to sell tickets through eventbrite

## General To Dos
- Add a public site roadmap as well as an SBESS roadmap

- Create scheduled backups
- Create database mirroring method with staging server
	- remove MAMP
-	install MariaDB
-	install Apache
( use virtual hostX for creating virtual hosts )

- Figure out why atrium feed isn't working
- Add the LaurierSBE tumblr feed

- Create a partial for latest tweets / tweet

- style tinyMCE ( WP backend )
- Search box at the top of the clubs page
	- implement typeahead
	- have a list of all clubs and a link to their page

- Add a map of laurier on the contact page

- Add the talent egg widget
- Learn how Aloha Edit works for input forms

## Future Integration
- Social social social
	- add Facebook popups for new visitors ( track with cookies )
	- casually suggest people follow us on twitter because "xzy"
	- deeper integration where possible

- Public calendar
	- allow people to add a public calendar to their laptops, phones, iPads etc. 
	- would show all the events data we have ( date, time, links )

## Structure of the site

SBESS
|--- your SBE
|    |--- new students
|    |    |--- mentorship
|    |--- mission statement
|    |--- the team
|    |--- accountability
|    |--- elections
|    |--- get involved ( link to hiring )
|--- student life
|    |--- student activities
|    |--- SBE co-op
|    |--- discussions
|    |--- get involved ( link to hiring )
|--- news
|    |--- new sources
|    |--- SBE weekly
|    |--- presidents blog
|    |--- atrium
|--- services
|    |--- events
|    |    |--- all upcoming events
|    |    |--- BUNCH
|    |    |--- O-day
|    |    |--- [•••] many others
|    |--- conferences and competitions
|    |    |--- directory
|    |    |--- apply for subsidy
|    |    |--- FAQs
|    |    |--- JDC Central
|    |--- student services
|    |    |--- clubs office booking
|    |    |--- student grievances
|    |    |--- submit announcements for SBE
|--- photos
|    |--- submit your own
|--- clubs
|    |--- directory
|    |--- get involved ( hiring )
|    |--- start a club at laurier
|--- store
|--- contact ( not in main menu )
|    |--- website feedback	
|--- corporate partners
|--- SEARCH
|
|--- login ( for internal site )
|--- Social ( modal )

## Pages

### Home Page
Our favourite page!

- Upcoming events
- Call-to-action banner
- Static welcome message
- Most Recents from the President's blog
- Corporate partners

- Heavy hitting social features

### New Students
Page is meant to provide links and resources for new students and access to the fantastic mentor program. It should also explain where to get information and how to get involved in SBE.

### Accountability
We want transparency and accountability at SBESS and in SBE as a whole. Here we are looking to post as much information as possible about the internal workings at SBE.

- Full council meeting minutes
- Exec meeting minutes ( maybe )
- Financials
	- Basic financial statements to allow students to know where the money is going and ask questions

- There will be a comments section on this page to encourage open discussion and en email to ask specific questions

### Elections
Since 2012, there has been an election for the incoming president for SBESS and this page manages all the information about the event.

- Candidate descriptions
	- Pictures
	- Social profiles
	- Platforms
- Important dates
- Important links
- Follow us on twitter for breaking news and updates

### News / Blogs

President's Blog
----------------
This section is meant to show up on the front page of the site as well as on the category page. The SBESS president is supposed to create 1 blog entry each month for posting. It is the IT director's repressibility to train them to use wordpress.

SBE Weekly
----------
Good'ol SBE weekly… We are currently working on way to automate this process — this process being the submission and delivery of SBE weekly updates; For now, this is a blog category and will show the most recent post in all footers.

### Events
The events home page will have a calendar displaying the month's events and will have links to the sub-pages of the events section

Upcoming Events
---------------
Chronological list of all the upcoming event data we have with links to their pages.

### Photos
This one was surprisingly fun to program… anyways, this flash gallery non-sense has got to go. Amazing CMS already good to go; no Masters of Computer Science needed to operate this go round.

We will be finding a jQuery gallery plugin and making this page beautiful. 完成

We're looking to at integrating a flickr API for photo management… just an idea for the future

### Clubs
This page will have a running list of all active clubs, we will aim to allow anyone in an appropriate position to make changes to the site themselves.

Info we keep on clubs

- Club Name
- Club Email
- Website URL
- Primary Contact Name
- Primary Contact Email
- Facebook URL
- Twitter URL
- Regular Meeting Times
- Club Logo
- Month / Year Founded
- Full Club Description
- Short Summary of Events

### Hiring
The hiring page is an essential part of the SBESS site, it must be easy to update and maintain with very little turnover.

- ACF form on admin to add,remove,update
- Option to place above, below or 'use short-code' to place the hiring form
- Automatic expiration for all jobs
- All data is collected in our database under 'jobs' table

+------------------+------------------+
| FIELDS AVAILABLE | ADMIN OPTIONS    |
+------------------+------------------+
| First Name       | Job Title        |
| Last Name        | Descriptions     |
| Program          | Requirements     |
| Year             | Co-op friendly   |
| Co-op            | Deadline         |
| Job 1            | Club             |
| Job 2            | Time Commitment  |
| Job 3            | Contact          |
| Question 1       | Term Length      |
| Question 2       | Interview Period |
| Question 3       |                  |
| Question 4       |                  |
| Additional Notes |                  |
+------------------+------------------+

### Corporate Partners
This page will not be changing too much, we had to create a system in the CMS to allow anyone to upload the all important CP Program PDF and have also added the ability to add logos from all our partners.