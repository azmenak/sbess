<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?>  <?php if ( !get_field( 'banner_deactivate' ) ) { ?>

<div class="banner">
  <?php if ( get_field( 'the_banner' ) ) { ?>

  <?php $first = true; ?>
<div id="theCarousel">
  .carousel.slide
<div class="carousel-inner">
  <?php while ( the_repeater_field( 'the_banner' ) ) { ?>

  <?php $class = $frist ? 'active item' : 'item'; ?>
  <?php if ( $first ) { ?>

  <?php $first = false; ?>
<?php } ?>
<div class="<?php echo $class; ?>">
<a href="<?php echo get_sub_field('banner_link'); ?>">
  <?php echo image_tag( wp_get_attachment_imgage( get_sub_field('banner_img' ) ) ); ?>

</a></div><?php } ?>
</div><a class="carousel-control left" data-slide="prev" href="#theCarousel">
  lsaquo;
</a><a class="carousel-control right" data-slide="next" href="#theCarousel">
  rsaquo;
</a></div><?php } ?>
<script type="text/javascript">
  //<![CDATA[
window.carousel_interval = parse_int( "<?php echo get_field('banner_interval'); ?>" )
// the execution is controlled in applicaton.coffee
  //]]>
</script>
</div><?php } ?>
