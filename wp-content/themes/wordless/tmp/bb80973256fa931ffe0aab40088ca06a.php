<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?>  <?php global $feed; ?>
  <?php if ( !is_wp_error( $feed ) ) { ?>

  <?php $maxitems = $feed->get_item_quantity(5); ?>
  <?php $feed_items = $feed->get_items(0, $maxitems); ?>
<?php } ?>
<h3>
  <?php echo link_to( $feed->get_title(), $feed->get_permalink() ); ?>

</h3><p>
  <?php echo $feed->get_description(); ?>

</p><ul>
  <?php foreach ( $feed_items as $news ) { ?>

<li>
  <?php echo link_to( esc_html( $news->get_title() ), esc_url( $news->get_permalink() ) ); ?>

</li><?php } ?>
</ul>