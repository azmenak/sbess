<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><div class="row-fluid">
<div class="span8">
  <?php render_partial('extras/breadcrumb'); ?>
  <?php render_partial('posts/page'); ?>
<div class="events">
  <?php $first = true; ?>
  <?php if ( get_field( 'events', 'option' ) ) { ?>

  <?php while( the_repeater_field( 'events', 'options' ) ) { ?>

  <?php $the_link = ( get_sub_field('event_external_link') ? get_sub_field('event_external_link') : get_sub_field('event_page') ); ?>
  <?php if ( $first ) { ?>

  <?php $first = false; ?>
  <?php } else { ?>

<hr /><?php } ?>
<div class="event" id="<?php echo str_replace( ' ', '-', get_sub_field( 'event_name' ) ); ?>">
<table cellpadding="6">
<tbody>
<tr>
<td>
  Event Name
</td>  <?php if ( get_sub_field('no_page') ) { ?>

<td>
  <?php echo get_sub_field('event_name'); ?>

</td>  <?php } else { ?>

<td>
  <?php echo link_to( get_sub_field('event_name'), $the_link ); ?>

</td><?php } ?>
</tr><tr>
<td>
  Location
</td><td>
  <?php echo get_sub_field('event_location'); ?>

</td></tr><tr>
<td>
  When
</td><td>
  <?php echo get_sub_field('event_date') . ' at ' . get_sub_field('event_time'); ?>

</td></tr></tbody></table><div class="description">
<div class="well">
  <?php echo get_sub_field('event_description'); ?>

</div></div>  <?php if ( !get_sub_field('no_page') ) { ?>

<p>
  <?php echo link_to( 'More Info &raquo;', $the_link ); ?>

</p><?php } ?>
</div><?php } ?>
<?php } ?>
</div></div><div class="span4">
<h3>
  List of Events
</h3>  <?php if ( get_field( 'events', 'options' ) ) { ?>

<ul>
  <?php while( the_repeater_field( 'events', 'option' ) ) { ?>

  <?php $name = get_sub_field( 'event_name' ); ?>
<li>
  <?php echo link_to( $name, '#' . str_replace( ' ', '-', $name ) ); ?>

</li><?php } ?>
</ul><?php } ?>
</div></div>