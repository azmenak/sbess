<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><div class="row-fluid">
<div class="span12">
<h2>
  The Awesome Gallery
</h2><div class="content">
  <?php render_partial('posts/page'); ?>
<div class="gallery">
  <?php if ( get_field('the_gallery') ) { ?>

  <?php while ( the_repeater_field('the_gallery') ) { ?>

<ul>
<h3>
  <?php echo get_sub_field('album_name'); ?>

</h3>  <?php $images = get_sub_field('album'); ?>
  <?php foreach( $images as $image ) { ?>

<li>
<a href="<?php echo $image['url']; ?>">
  <?php echo image_tag( $image['sizes']['thumbnail'] ); ?>

</a></li><?php } ?>
</ul><?php } ?>
<?php } ?>
</div></div></div></div>