<?php
require_once '/Users/az/Dropbox/Sites/sbess_wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><meta content="chrome=1" http-equiv="X-UA-Compatible" /><meta content="text/html;charset=UTF-8" http-equiv="Content-type" /><meta content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" name="viewport" />  <?php echo stylesheet_link_tag("screen"); ?>

  <?php echo favicon_link_tag(); ?>

<title>
  <?php echo bloginfo('name'); ?>

  <?php wp_title(); ?>
</title>  <?php if ( ifie::_boole() ) { ?>

  <?php echo render_partial("layouts/ie_vars"); ?>

<?php } ?>
  <?php global $post; ?>
<script type="text/javascript">
  //<![CDATA[
var THEMEPATH = "<?php echo get_template_directory_uri(); ?>";
var MOBILE    = !!"<?php echo ifmobile::mobile(); ?>";
var POST      = <?php echo json_encode( $post ); ?>;
var ISIE      = <?php echo ifie::_boole(); ?>;

hasClass = function(element, cls) {
  r = new RegExp('\\b' + cls + '\\b');
  return r.test(element.className);
}

window.jspath = THEMEPATH + '/assets/javascripts/';
window.csspath = THEMEPATH + '/assets/stylesheets/';
window.flash = {};
  //]]>
</script>
  <?php echo javascript_include_tag("modernizr.min"); ?>

  <?php echo javascript_include_tag("jquery.min.js"); ?>

  <?php echo javascript_include_tag("library.min.js"); ?>

  <?php echo javascript_include_tag("application.js"); ?>

  <?php wp_head(); ?>
