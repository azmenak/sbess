<?php
require_once '/Users/az/Dropbox/Sites/sbess_wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?>  <?php if ( !get_field( 'hero_deactivate') ) { ?>

<div class="hero-unit">
<h1>
  <?php echo get_field( 'hero_heading' ); ?>

</h1><p>
  <?php echo get_field( 'hero_tagline' ); ?>

</p><p>
  <?php $args['class'] = 'btn btn-primary btn-large'; ?>
  <?php echo link_to( get_field( 'hero_btn_label' ), get_field( 'hero_link' ), $args ); ?>

</p></div><?php } ?>
