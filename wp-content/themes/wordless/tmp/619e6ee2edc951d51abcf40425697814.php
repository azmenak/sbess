<?php
require_once '/Users/az/Dropbox/Sites/sbess_wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?>  <?php if ( !get_field( 'banner_deactivate' ) ) { ?>

<div class="banner">
  <?php if ( get_field( 'the_banner' ) ) { ?>

  <?php $first = true; ?>
<div class="carousel slide" id="theCarousel">
<div class="carousel-inner">
  <?php while ( the_repeater_field( 'the_banner' ) ) { ?>

  <?php $class = $first ? 'active item' : 'item'; ?>
  <?php if ( $first ) { ?>

  <?php $first = false; ?>
<?php } ?>
<div class="<?php echo $class; ?>">
  <?php $image = get_sub_field( 'banner_image' ); ?>
  <?php if ( get_sub_field( 'banner_link' ) ) { ?>

<a href="<?php echo get_sub_field( 'banner_link' ); ?>">
  <?php echo image_tag( $image['sizes']['banner'] ); ?>

</a>  <?php } else { ?>

  <?php echo image_tag( $image['sizes']['banner'] ); ?>

<?php } ?>
</div><?php } ?>
</div><a class="carousel-control left" data-slide="prev" href="#theCarousel">
  <?php echo '&lsaquo;'; ?>

</a><a class="carousel-control right" data-slide="next" href="#theCarousel">
  <?php echo '&rsaquo;'; ?>

</a></div><?php } ?>
<script type="text/javascript">
  //<![CDATA[
if ( !"<?php echo get_field( 'banner_deactivate' ); ?>" ) {
  window.carousel_interval = parseInt( "<?php echo get_field('banner_interval'); ?>" )
}
  //]]>
</script>
</div><?php } ?>
