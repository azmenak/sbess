<?php
require_once '/Users/az/Dropbox/Sites/sbess/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><meta content="text/html;charset=UTF-8" http-equiv="Content-type" /><title>
  <?php echo get_page_title(bloginfo('name'), " – "); ?>

</title>  <?php $di_path = get_template_directory_uri(); ?>
  <?php echo render_partial("layouts/ie_vars"); ?>

<script type="text/javascript">
  //<![CDATA[
var THEMEPATH = '<?php echo $di_path; ?>';
var MOBILE = !!'<?php echo ifmobile::mobile(); ?>';

  //]]>
</script>
  <?php echo stylesheet_link_tag("screen"); ?>

  <?php echo javascript_include_tag("modernizr.min"); ?>

  <?php echo javascript_include_tag("loader"); ?>

  <?php wp_head(); ?>
