<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><!DOCTYPE html>
  <?php echo render_partial("layouts/ie_html"); ?>

<html class="<?php echo ifmobile::test(); ?>" lang="en">
<head>
  <?php echo render_partial("layouts/head"); ?>

</head><body class="<?php echo body_class(); ?>">
<div class="site-wrapper">
<div class="site-header-content">
<header class="site-header">
  <?php echo render_partial("layouts/header"); ?>

</header><section class="site-content">
<div class="container-fluid">
  <?php echo yield(); ?>

</div></section></div><footer class="site-footer">
  <?php echo render_partial("layouts/footer"); ?>

</footer></div></body></html>