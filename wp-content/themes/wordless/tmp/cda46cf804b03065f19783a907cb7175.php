<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?>  <?php $s = get_search_query(); ?>
<div class="row-fluid">
<div class="span9">
  <?php render_partial('extras/breadcrumb'); ?>
<h2>
  Search Results for "<?php echo $s; ?>"
</h2><div class="results">
  <?php if ( have_posts() ) { ?>

  <?php while( have_posts() ) { ?>

<hr />  <?php the_post(); ?>
  <?php $title = get_the_title(); ?>
  <?php $keys = explode( " ", $s ); ?>
  <?php $title = preg_replace( '/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">\0</strong>', $title); ?>
<div class="post">
<header>
<h4>
  <?php echo link_to( $title, get_permalink() ); ?>

  <?php if ( get_post_type() == 'post' ) { ?>

<span>
  <?php echo '&raquo;'; ?> Blog Post
</span>  <?php } else if ( get_post_type() == 'page' ) { ?>

<span>
  <?php echo '&raquo;'; ?> Site Page
</span><?php } ?>
</h4><p>
  <?php echo get_the_date(); ?>

</p></header><div class="content">
  <?php echo get_the_excerpt(); ?>

</div></div><?php } ?>
  <?php } else { ?>

<p>
  Sorry, but nothing matched your search criteria, you may want to try a suggested search or a new search.
</p><?php } ?>
</div></div><div class="span3">
<h3>
  Common Searches
</h3><ul>
<li>
  <?php echo link_to( 'Clubs Office Booking', '/?s=clubs+office+booking' ); ?>

</li><li>
  <?php echo link_to( 'Contact Us', '/?s=contact+us' ); ?>

</li></ul><h3>
  Related Searches
</h3><p>
  We are currently working on implementing
  related searches...
</p></div></div>