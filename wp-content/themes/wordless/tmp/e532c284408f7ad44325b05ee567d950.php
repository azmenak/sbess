<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><div class="row-fluid">
<div class="span12">
  <?php render_partial('extras/hero_unit'); ?>
  <?php render_partial('extras/banner'); ?>
</div></div><div class="row-fluid">
<div class="span9">
  <?php render_partial( 'extras/breadcrumb' ); ?>
<div class="welcome">
<h3>
  Welcome Message
</h3>  <?php render_partial('posts/page'); ?>
</div>  <?php $posts_on_page = intval( get_field('number_of_posts') ); ?>
<div class="blog">
<h3>
  President's Blog
<small>
  <?php echo link_to( 'see the archives &raquo;', '/category/president-blog/' ); ?>

</small></h3>  <?php global $post; ?>
  <?php $the_posts = get_posts( array( 'numberposts' => $posts_on_page, 'category' => get_category_id('President Blog' ) ) ); ?>
  <?php foreach ( $the_posts as $post ) { ?>

  <?php setup_postdata( $post ); ?>
  <?php render_partial('posts/post'); ?>
<?php } ?>
</div></div><div class="span3">
<h2>
  Upcoming Events
</h2><p>
  The Most exciting website launch ever
</p><h2>
  Coporate Partners
</h2><p>
  Our corporate partners are the very best,
  check out the
  <?php echo link_to( 'CPP Page', get_permalink(128) ); ?>

  for more information about the program
</p><ul class="thumbnails">
  <?php $photos = get_field('partners_logos', 128); ?>
  <?php if ( $photos ) { ?>

  <?php foreach( $photos as $photo ) { ?>

<li class="span1">
<a class="thumbnail" href="<?php echo $photo['alt']; ?>">
<img src="<?php echo $photo['sizes']['tiny']; ?>" /></a></li><?php } ?>
<?php } ?>
</ul></div></div>