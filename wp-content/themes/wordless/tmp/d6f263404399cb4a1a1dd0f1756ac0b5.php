<?php
require_once '/Users/az/Dropbox/Sites/sbess_wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><div class="row">
<div class="span12">
<h2>
  Awwww snap! 404 :(
</h2><div class="content">
<p>
  We hate these errors as much as the next guy,
  if you think there is something seriously wrong here
  please let us know by sending an email to
  <?php echo link_to( 'admin@sbess.ca', mailto('admin@sbess.ca', '404 Error - ' . $_SERVER['REQUEST_URI'] ) ); ?>

  and we will be getting right on that.
</p><p>
  Thanks for your help
</p></div></div></div>