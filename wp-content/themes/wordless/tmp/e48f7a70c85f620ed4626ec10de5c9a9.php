<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><hr /><div class="container-fluid">
<div class="row-fluid">
<div class="span3">
<div class="sbeweekly">
<h2>
  <?php echo link_to( 'SBE Weekly Latest &raquo;', '/category/sbe-weekly/' ); ?>

</h2>  <?php global $post; ?>
  <?php $the_posts = get_posts( array( 'numberposts' => 1, 'category' => get_category_id( 'SBE Weekly' ) ) ); ?>
  <?php foreach ( $the_posts as $post ) { ?>

  <?php setup_postdata( $post ); ?>
  <?php render_partial('posts/excerpt'); ?>
<?php } ?>
</div></div><div class="span6">
<nav>
  <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
</nav></div><div class="span3">
<div class="social">
<div class="twitter">
  <?php echo link_to( 'Twitter', 'https://twitter.com/LaurierSBESS' ); ?>

</div><div class="facebook">
  <?php echo link_to( 'Facebook', 'http://www.facebook.com/LaurierSBESS' ); ?>

</div></div></div></div><div class="row-fluid">
<div class="span12">
<p>
  Developed by
  <?php echo link_to( 'dInteractive', 'http://dinteractive.ca' ); ?>

</p></div></div></div>  <?php wp_footer(); ?>
