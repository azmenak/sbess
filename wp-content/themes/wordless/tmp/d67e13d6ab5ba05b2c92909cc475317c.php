<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><div class="row-fluid">
<div class="span3">
<h3>
  List of Clubs
</h3><ul>
  <?php if ( get_field( 'clubs_list' ) ) { ?>

  <?php while( the_repeater_field( 'clubs_list' ) ) { ?>

<li>
  <?php echo link_to( get_sub_field( 'club_name' ), '#' . str_replace( ' ', '-', get_sub_field( 'club_name' ) ) ); ?>

</li><?php } ?>
<?php } ?>
</ul></div><div class="span9">
  <?php render_partial( 'extras/breadcrumb' ); ?>
<div class="static-content">
<div class="alert alert-info">
  <?php echo render_partial( 'posts/page' ); ?>

</div></div><div class="clubs">
  <?php $first = true; ?>
  <?php if ( get_field( 'clubs_list' ) ) { ?>

  <?php while( the_repeater_field( 'clubs_list' ) ) { ?>

  <?php if ( $first ) { ?>

  <?php $first = false; ?>
  <?php } else { ?>

<hr /><?php } ?>
<div class="club" id="<?php echo str_replace( ' ', '-', get_sub_field( 'club_name' ) ); ?>">
  <?php if ( get_sub_field( 'club_logo' ) ) { ?>

  <?php echo wp_get_attachment_image( get_sub_field( 'club_logo' ), 'small' ); ?>

<?php } ?>
<table cellpadding="6">
<tbody>
<tr>
<td>
  Name
</td><td>
  <?php echo get_sub_field( 'club_name' ); ?>

</td></tr>  <?php if ( get_sub_field( 'club_email' ) ) { ?>

<tr>
<td>
  Email
</td>  <?php $message = 'SBESS - ' . get_sub_field( 'club_name' ) . ' inquiry'; ?>
<td>
  <?php echo link_to( get_sub_field( 'club_email' ), mailto( get_sub_field( 'club_email' ), $message )  ); ?>

</td></tr><?php } ?>
  <?php if ( get_sub_field( 'website_url' ) ) { ?>

<tr>
<td>
  Website
</td><td>
  <?php echo link_to( get_sub_field( 'website_url' ), get_sub_field( 'website_url' ) ); ?>

</td></tr><?php } ?>
  <?php if ( get_sub_field( 'short_club_description' ) ) { ?>

<tr>
<td>
  Description
</td><td>
  <?php echo get_sub_field( 'short_club_description' ); ?>

</td></tr><?php } ?>
</tbody></table></div><?php } ?>
<?php } ?>
</div></div></div>