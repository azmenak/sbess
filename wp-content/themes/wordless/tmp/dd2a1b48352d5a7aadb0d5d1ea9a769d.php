<?php
require_once '/Users/az/Dropbox/Sites/sbess wordless/wp-content/plugins/welaika-wordless/vendor/phamlp/haml/HamlHelpers.php';
?><div class="row-fluid">
<div class="span8">
<div class="business-news">
<div class="row-fluid">
<div class="span12">
  <?php render_partial( 'extras/breadcrumb' ); ?>
<div class="static-content">
  <?php echo render_partial('posts/page'); ?>

</div></div></div><div class="row-fluid">
  <?php $feeds['financial_post'] = fetch_feed( 'http://www.financialpost.com/16994.rss' ); ?>
  <?php $feeds['canadian_business'] = fetch_feed( 'http://www.canadianbusiness.com/rss/business.xml' ); ?>
  <?php $feeds['economist'] = fetch_feed( 'http://www.economist.com/rss/daily_news_and_views_rss.xml' ); ?>
  <?php $sides['left'] = $feeds; ?>
  <?php $feeds = array(); ?>
  <?php $feeds['cbc_news'] = fetch_feed( 'http://rss.cbc.ca/lineup/topstories.xml' ); ?>
  <?php $feeds['bbc_world'] = fetch_feed( 'http://newsrss.bbc.co.uk/rss/newsonline_world_edition/front_page/rss.xml' ); ?>
  <?php $sides['right'] = $feeds; ?>
  <?php foreach( $sides as $feeds )        { ?>

<div class="span6">
  <?php foreach( $feeds as $feed ) { ?>

  <?php if ( !is_wp_error( $feed ) ) { ?>

  <?php $maxitems = $feed->get_item_quantity(5); ?>
  <?php $feed_items = $feed->get_items(0, $maxitems); ?>
<?php } ?>
<h3>
  <?php echo link_to( $feed->get_title(), $feed->get_permalink() ); ?>

</h3><p>
  <?php echo $feed->get_description(); ?>

</p><ul>
  <?php foreach ( $feed_items as $news ) { ?>

<li>
  <?php echo link_to( esc_html( $news->get_title() ), esc_url( $news->get_permalink() ) ); ?>

</li><?php } ?>
</ul><?php } ?>
</div><?php } ?>
</div></div></div><div class="span4">
<div class="blogs">
<h3>
  President's Blog
  <?php echo link_to( 'see the archives &raquo;', '/category/president-blog/' ); ?>

</h3>  <?php global $post; ?>
  <?php $the_posts = get_posts( array( 'numberposts' => 3, 'category' => get_category_id( 'President Blog' ) ) ); ?>
  <?php foreach ( $the_posts as $post ) { ?>

  <?php setup_postdata( $post ); ?>
  <?php render_partial('posts/excerpt'); ?>
<?php } ?>
<hr /><h3>
  SBE Weekly
  <?php echo link_to( 'see the archives &raquo;', '/category/sbe-weekly/' ); ?>

</h3>  <?php global $post; ?>
  <?php $the_posts = get_posts( array( 'numberposts' => 5, 'category' => get_category_id( 'SBE Weekly'  ) ) ); ?>
  <?php foreach ( $the_posts as $post ) { ?>

  <?php setup_postdata( $post ); ?>
  <?php render_partial('posts/excerpt'); ?>
<?php } ?>
<hr /><h3>
  Atrium Media Group
</h3><p>
  The atrium feed isn't working, so until
  they sort their shit out this will say
  here instead.
</p></div></div></div>