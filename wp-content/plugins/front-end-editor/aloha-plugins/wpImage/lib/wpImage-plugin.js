define(['aloha/plugin', 'aloha/floatingmenu', 'i18n!aloha/nls/i18n'], function(Plugin, FloatingMenu, i18nCore){
  var Aloha;
  Aloha = window.Aloha;
  return Plugin.create('wpImage', {
    init: function(){
      var button;
      button = new Aloha.ui.Button({
        'name': 'wpImage',
        'iconClass': 'ImageWP',
        'size': 'small',
        'onclick': __bind(this, this.insert),
        'tooltip': FrontEndEditor.data.image.insert
      });
      return FloatingMenu.addButton('Aloha.continuoustext', button, i18nCore.t('floatingmenu.tab.insert'), 2);
    },
    insert: function(){
      var instance;
      instance = new FrontEndEditor.fieldTypes.image_rich;
      return instance.start_editing();
    }
  });
});
function __bind(me, fn){ return function(){ return fn.apply(me, arguments) } }