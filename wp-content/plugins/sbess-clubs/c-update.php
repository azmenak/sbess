<?php

require 'c-db.php';

global $wpdb;
global $table_name;
global $img_dir;

require_once( $_GET['abs'] . '/wp-load.php' );

$large    = $_FILES['large'];
$thumb    = $_FILES['thumb'];
$banner   = $_FILES['banner'];

$id       = $_GET['id'];
$name     = $_GET['name'];
$price    = $_GET['price'];
$regular  = $_GET['regular'];

$species  = $_GET['species'];
$colour   = $_GET['colour'];
$width    = $_GET['width'];
$grade    = $_GET['grade'];
$type     = $_GET['type'];
$quantity = $_GET['quantity'];
$sku      = $_GET['sku'];
$notes    = $_GET['notes'];

if ( $id == -1 ) {
	lg_add_new();
} else {
	lg_update();
}

function lg_add_new() {

	$large    = $_FILES['large'];
	$thumb    = $_FILES['thumb'];
	$banner   = $_FILES['banner'];

	$id       = $_GET['id'];
	$name     = $_GET['name'];
	$price    = $_GET['price'];
	$regular  = $_GET['regular'];

	$species  = $_GET['species'];
	$colour   = $_GET['colour'];
	$width    = $_GET['width'];
	$grade    = $_GET['grade'];
	$type     = $_GET['type'];
	$quantity = $_GET['quantity'];
	$sku      = $_GET['sku'];
	$notes    = $_GET['notes'];

	if ( isset($price) && $large['error']<1 && $thumb['error']<1 && $banner['error']<1 ) {

		$row = array(
			'name'     => $name,
			'price'    => $price,
			'regular'  => $regular,
			'species'  => $species,
			'colour'   => $colour,
			'width'    => $width,
			'grade'    => $grade,
			'type'     => $type,
			'quantity' => $quantity,
			'sku'      => $sku,
			'notes'    => $notes,
			'thumb'    => $thumb['name'],
			'large'    => $large['name'],
			'banner'   => $banner['name']
		);
		$wpdb->insert( $table_name, $row );

		move_uploaded_file($thumb['tmp_name'], $img_dir.'/'.$thumb['name']);
		move_uploaded_file($large['tmp_name'], $img_dir.'/'.$large['name']);
		move_uploaded_file($banner['tmp_name'], $img_dir.'/'.$banner['name']);

		echo $wpdb->insert_id;

	} else {
		echo '-1';
	}
}

function lg_update() {

	$large    = $_FILES['large'];
	$thumb    = $_FILES['thumb'];
	$banner   = $_FILES['banner'];

	$id       = $_GET['id'];
	$name     = $_GET['name'];
	$price    = $_GET['price'];
	$regular  = $_GET['regular'];

	$species  = $_GET['species'];
	$colour   = $_GET['colour'];
	$width    = $_GET['width'];
	$grade    = $_GET['grade'];
	$type     = $_GET['type'];
	$quantity = $_GET['quantity'];
	$sku      = $_GET['sku'];
	$notes    = $_GET['notes'];

	if ( isset($price) && $large['error']<1 && $thumb['error']<1 && $banner['error']<1 ) {
		if ( $thumb['size'] > 3 ) {
			$file = $thumb['name'];
			$sql = "UPDATE $table_name SET thumb='$file' WHERE id=$id;";
			$sql = $wpdb->prepare( $sql );
			$wpdb->query( $sql );
			move_uploaded_file($thumb['tmp_name'], $img_dir.'/'.$thumb['name']);
		}
		if ( $large['size'] > 3 ) {
			$file = $large['name'];
			$sql = "UPDATE $table_name SET large='$file' WHERE id=$id;";
			$sql = $wpdb->prepare( $sql );
			$wpdb->query( $sql );
			move_uploaded_file($large['tmp_name'], $img_dir.'/'.$large['name']);
		}
		if ( $banner['size'] > 3 ) {
			$file = $banner['name'];
			$sql = "UPDATE $table_name SET banner='$file' WHERE id=$id;";
			$sql = $wpdb->prepare( $sql );
			$wpdb->query( $sql );
			move_uploaded_file($banner['tmp_name'], $img_dir.'/'.$banner['name']);
		}

		$sql = "UPDATE $table_name SET
				name='$name',
				price='$price',
				regular='$regular',
				species='$species',
				colour='$colour',
				width='$width',
				grade='$grade',
				type='$grade',
				quantity='$quantity',
				sku='$sku',
				notes='$notes'

			WHERE id=$id;";

		$sql = $wpdb->prepare($sql);
		$wpdb->query( $sql );

		echo $id;
	} else {
		echo '-1';
	}
}

?>