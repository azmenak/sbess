<?php

/*
 * Retreive a list of all the clubs ever entered
 * Offers the option to restore them as well
 */

require_once('c-db.php');

global $wpdb;
$clubsdb = new clubsdb();

$table_name = $clubsdb->table_name();

$sql = "SELECT * FROM $table_name ORDER BY `id`";
$sql = $wpdb->prepare($sql);
$rows = $wpdb->get_results($sql);

foreach ($rows as $row):

?>

<form action="<?php echo $update_dir; ?>" enctype="multipart/form-data" method="get">

	<input type="hidden" name="action" value="restore" />
	<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
	<input type="hidden" name="abs" value="<?php echo ABSPATH; ?>">
	
	<div class="postbox">
		<h3>
			<?php echo $row->club_name; ?>, Club ID: <?php echo $row->id; ?>
			<?php if ($row->deleted == 1): ?>
				<span><button class="button-primary">Restore</button></span>
			<?php endif; ?>
		</h3>
	</div>
</form>

<?php endforeach; ?>