<?php

/*
 * Plugin Name: SBESS Clubs
 * Description: SBESS Clubs List Update and Display
 * Version: 1.0
 * Author: Digital Interactive
 * Author URI: http://dinteractive.ca
 */

require_once('c-db.php'); // pulls in the vars used accross files

global $wpdb;
$clubsdb = new clubsdb( $wpdb );

//echo '<br><br><br>Start My Output:<br><br>';
//echo $clubsdb->update_dir();
//echo '<br><br>End My Output.<br><br>';

/*
 * ::clubs class
 * A Static class containing all the init and call
 * functions for the plugin
 * 
 * when referencing functions from outside the class
 * use clubs::function_name()
 */

class clubs {

	/*
	 * Query the database for all the club values
	 * and return a large string with the assocaited
	 * markup to be rendered by another script
	 */

	static function get_clubs() {
		global $wpdb;
		$clubsdb = new clubsdb();

		$table_name = $clubsdb->table_name();
		$img_dir = $clubsdb->img_dir();

		$sql = "SELECT * FROM $table_name";
		$sql = $wpdb->prepare( $sql ); 

		$rows = $wpdb->get_results( $sql );
		$spots = "";

		foreach ( $rows as $row ):

			$s01 = $img_dir.'/'.$row->banner;
			$s02 = $img_dir.'/'.$row->large;
			$s03 = $row->name;
			$s04 = $img_dir.'/'.$row->thumb;
			$s05 = $row->price;
			$s06 = $row->regular;
			$s07 = $row->species;
			$s08 = $row->colour;
			$s09 = $row->width;
			$s10 = $row->grade;
			$s11 = $row->type;

			$s12 = $row->quantity;
			$s13 = $row->sku;
			$s14 = $row->notes;

$spots .= <<<SPOT
<div itemscope itemtype="http://schema.org/Product" class="product">
	<img src="$s01" alt="Gallery Banner" class="banner">
	<a href="$s02" title="$s03" class="img fancybox" >
		<img src="$s04" alt="Product Thumbnail" class="sample">
	</a>
	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="info">
		<p class="price">
			<span class="dollar">$</span><span>$s05</span><span class="sqft">/sq.ft.</span>
		</p>
		<p class="regular">
			<span>Regular: <span class="dollar">$</span><span>$s06</span><span class="sqft">/sq.ft.</span>
		</p>
		<table>
			<tbody>
				<tr>
					<td>Species:</td>
					<td>$s07</td>
				</tr>
				<tr>
					<td>Colour:</td>
					<td>$s08</td>
				</tr>
				<tr>
					<td>Width:</td>
					<td>$s09</td>
				</tr>
				<tr>
					<td>Grade:</td>
					<td>$s10</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
SPOT;

		endforeach;

		return $spots;
	}

	/*
	 * Package and send the markup to the front end
	 * is rendered by one of the hooks at the top
	 * of this script
	 */

	static function render_frontend() {
		$clubs = get_clubs();
		$input = '<div id="clubs-list">'.$clubs.'</div>';
		return $input;
	}

	/*
	 * Create the database table on install if it dosen't
	 * already exist and updates the table if more fields
	 * are added
	 */

	static function install() {
		global $wpdb;
		$clubsdb = new clubsdb();

		$table_name = $clubsdb->table_name();
		
		$sql = "CREATE TABLE $table_name (
		  		id INTEGER(9) NOT NULL AUTO_INCREMENT,
		  		order INTEGER(9) NOT NULL AUTO_INCREMENT,
					image VARCHAR(256) NOT NULL,
					club_name VARCHAR(256) NOT NULL,
					club_email VARCHAR(256) NOT NULL,
					website VARCHAR(512) NOT NULL,
					description VARCHAR(2048) NOT NULL,
					contact_name VARCHAR(256) NOT NULL,
					contact_email VARCHAR(256) NOT NULL,
					date_added TIMESTAMP NOT NULL,
					date_mod TIMESTAMP NOT NULL,
					facebook VARCHAR(256) NOT NULL,
					twitter VARCHAR(256) NOT NULL,
					g_plus VARCHAR(256) NOT NULL,
					year_founded INTEGER(9) NOT NULL,
					month_founded INTEGER(4) NOT NULL,
					major_events VARCHAR(2049) NOT NULL,
					meeting_time VARCHAR(256) NOT NULL,
					accepting INTEGER(3) NOT NULL,
					collapsed INTEGER(2) NOT NULL,
					active INTEGER(2) NOT NULL,
					deleted INTEGER(2) NOT NULL,
	  				UNIQUE KEY id (id)
				);";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		
		// this isn't working. fix it.
		
		$wpdb->get_row( "SELECT * FROM $table_name WHERE id = -1" );
		
		if ( $wpdb->num_rows > 0 ) {
			$row = array( 'id' => -1 );
			$wpdb->insert(
				$table_name,
				$row 
			);
		}
	}

	/*
	 * Creates an entry in the wordpress backend
	 * and required users have the edit_clubs_list
	 * capability to see it, then registers the run
	 * function to execute when the backend loads
	 */

	static function add_to_menu() {
		$page_title = 'SBESS Clubs List';
		$menu_title = 'Clubs';
		$capability = 'edit_clubs_list';
		$menu_slug = 'edit-clubs-list';

		$pagehook = add_menu_page(
			$page_title, 
			$menu_title, 
			$capability, 
			$menu_slug, 
			$function =  array('clubs', 'render_backend')
		);

		add_action( 'admin_head-' . $pagehook, array('clubs', 'run') );
	}

	/*
	 * loads the backend markup
	 * Called by the add_menu_page action
	 */
	 
	static function render_backend() {
		include 'c-backend.php';
	}

	/*
	 * Register the backend scripts with it's dependecies
	 * to be loaded in run and rregister the CSS for the
	 * backend
	 */
	static function register(){
		wp_register_script(
			'bootstrap-js',
			plugins_url('bootstrap/js/bootstrap.min.js', __FILE__),
			array( 'jquery' )
		);
	
		$deps = array(
			'jquery', 'jquery-form', 'bootstrap-js'
		);
		wp_register_script(
			'clubs-scripts',
			plugins_url('c-scripts.js', __FILE__),
			$deps
		);

		wp_register_style(
			'twitter-bootstrap',
			plugins_url('bootstrap/css/bootstrap.min.css', __FILE__)
		);

		wp_register_style(
			'clubs-style',
			plugins_url('c-backend-style.css', __FILE__)
		);
	}

	/*
	 * Execute the js
	 * load the CSS
	 */
	 
	static function run() {
		wp_print_scripts( 'clubs-scripts' );
		wp_print_styles( 'clubs-style' );
		//wp_print_styles( 'twitter-bootstrap' );
	}

} // close the class

add_action( 'admin_init', array('clubs', 'register') );
add_action( 'admin_menu', array('clubs', 'add_to_menu') );
add_shortcode( 'clubs_list', array('clubs', 'render_frontend') );
register_activation_hook( __FILE__, array('clubs', 'install') );

?>