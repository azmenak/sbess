jQuery( document ).ready ($) ->
	
	#
	# Init for the modal
	# 
	
	$('#modal').modal show : false

	#
	# Takes the cloner element out of the DOM 
	# and into memory
	#

	clone = $('#loner').children().clone()
	clone.hide()
	$('#loner').remove()

	#
	# Create a new block when the add block
	# button is clicked, adds the handler to the
	# lowest level element that does not change
	# 

	$('#wpbody-content').on 'click', 'input.add-block', (e) ->
		block_count = $('#ss form').length 
		clone.find('h3').html 'block' + (block_count + 1)
		tmp = clone.clone()
		$(this).before tmp 
		tmp.slideDown 400 

	#
	# Sends an AJAX request when the update
	# button is clicked, adds the handler to the
	# lowest level element that does not change
	# 

	$('#wpbody-content').on 'click', '#ss .update', (e) ->
		e.preventDefault()
		block = $(this).closest 'form'
		block.ajaxForm()
		block.animate opacity : 0.4
		$(this).attr disabled : 'disabled'

		if block.find('h3').data('collapsed')
			block.find('[name="collapsed"]').val 1
		else
			block.find('[name="collapsed"]').val 0

		block.ajaxForm (data) =>
			$(this).removeAttr 'disabled'
			block.children().first().attr value : data
			block.animate opacity : 1
			alert 'Block Update Successful'

	#
	# Deletes a block when the delete button is
	# clicked, sends an AJAX request if the item
	# is currently in the database, adds the
	# handler to the lowest level element that
	# does not change
	#

	$('#wpbody-content').on 'click', '#ss .deletion', (e) ->
		e.preventDefault()
		block = $(this).closest 'form'
		
		alert "hi"
		
		$('#modal').modal 'show'
		
		msg = false;
		#msg = 'Are you sure you want to delete this block?\rTHIS CANNOT BE UNDONE'
		if msg
			if block.find('input[name="id"]').val() == -1
				block.slideUp 400, () ->
					$(this).remove()
			else
				block.find('[name="action"]').val('delete')
				post = $(this).data 'deleteurl'
				post = post.substring(0,post.length-1) + '$id=' + block.find('input[name="id"').val()
				$.get( post )
				.success (data) ->
					console.log data
					block.slideUp 400, () ->
						$(this).remove()
				.error ->
					alert( 'There was an error communicating with the server' )
				.complete ->
					block.find('[name="action"]').val('update')

	#
	# Handle the collapsing and reordering
	# Used jQuery UI sortable
	#

	$('#wpbody-content').on 'click', '#ss h3', (e) ->
		if running then return false

		collapsed = !!$(this).data 'collapsed'

		inside = $(this).next()
		time = 200

		if !collapsed
			running = true
			inside.slideUp time, () =>
				running = false
				$(this).data collapsed : true
				$(this).children().html 'Click to Expand'
				$(this).css cursor : 'move'

		else
			running = true
			inside.slideDown time, () =>
				running = false
				$(this).data collapsed : false
				$(this).children().html 'Click to Collapse'
				$(this).css cursor : 'pointer'

