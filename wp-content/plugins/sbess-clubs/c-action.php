<?php

/*
 * The super fun file that does everything
 * uses the action param for a swtich statement
 */

require_once('c-db.php');
require_once( $_GET['a'] . '/wp-load.php' );

global $wpdb;
$clubsdb = new clubsdb();
$table_name = $clubsdb->table_name;

$action = $_GET['action'];

switch ($action) {
	case 'delete':

		$id = $_GET['id'];
		$sql = "UPDATE $table_name SET deleted=1 WHERE id=$id";
		$sql = $wpdb->prepare($sql);
		$wpdb->query($sql);
		
		echo "Removed club with id: $id";

		break;
	
	case 'restore':

		$id = $_GET['id'];
		$sql = "UPDATE $table_name SET deleted=0 WHERE id=$id";
		$sqp = $wpdb->prepare($sql);
		$wpdb->query($sql);
		
		echo "Restored club with id: $id";

		break;
		
	case 'update':
		
		$id = $_GET['id'];		
		$image = $_FILES['image'];

		$row = array(
			'club_name' => $_GET['name'];,
			'club_email' => $_GET['email'],
			'website' => $_GET['web'],
			'description' => $_GET['des'],
			'contact_name' => $_GET['cname'],
			'contact_email' => $_GET['cemail'],
			'facebook' => $_GET['fb'],
			'twitter' => $_GET['twitter'],
			'g_plus' => $_GET['gplus'],
			'year_founded' => $_GET['year'],
			'month_founded' => $_GET['month'],
			'major_events' => $_GET['major'],
			'meeting_time' => $_GET['meeting'],
			'accepting' => $_GET['accepting'],
			'collapsed' => $_GET['collapsed']
		);
		
		/*
		 * Add a new entry into the database
		 * check for error on the image
		 * ensure the interger values are set
		 */
		 
		if ( $id == -1 ):
		
			$row['date_added'] = now();
		
			if (isset($year) && isset($month) && $image['error'] < 1) {
			
				if ( $image['size'] > 3 ) {
					$file = $image['name'];
					$row['image'] = $file;
					move_uploaded_file( 
						$image['tmp_name'],
						$clubsdb->img_dir() . '/' . $file
					);
				}
				$wpdb->update(
					$table_name,
					$row,
					array( 'id' => $id )
				);
				echo $id;
				
			} else {
				echo '-1';
			}
		
		endif;
		
		/*
		 * Update the databse with the new info
		 * again checks for image and int values
		 */
		
		else:
		
			if (isset($year) && isset($month) && $image['error'] < 1) {
			
				$row['image'] = $image['name'];
				move_uploaded_file(
					$image['tmp_name'],
					$clubsdb->img_dir() . '/' . $image['name']
				);
				$wpdb->insert(
					$table_name, 
					$row
				);
				echo $wpdb->insert_id;
				
			} else {
				echo '-1';
			}
		
		endif;
		
		break;

	default:
		echo "Invalid Call";
		break;
}

?>