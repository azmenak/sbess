<?php

/*
 * Backend GUI for modifying the Clubs List
 * Author: dInteractive
 * Dependencies: jQuery, jQuery Ajax Form
 */

require_once('c-db.php');

global $wpdb;
$clubsdb = new clubsdb();

$table_name = $clubsdb->table_name();
$action_dir = $clubsdb->action();

$img_dir = $clubsdb->img_dir();

$sql = "SELECT * FROM $table_name WHERE deleted=0 ORDER BY `id`";
$sql = $wpdb->prepare($sql);
$rows = $wpdb->get_results($sql);

?>

<div class="wrap column-2">
	<div class="icon32" id="icon-themes"><br /></div>
	<h2>SBESS Clubs Manager</h2>
	<div class="metabox-holder has-right-sidebar">
		<div class="inner-sidebar">
			<div class="meta-box-sortables">
				<div class="postbox">
					<h3>Publish</h3>
					<div class="inside">
						<p>You may include styling in the larger textboxes by using markup tags: 
						<br><br>
						<code>
							CODE
							<br>STRONG
							<br>I
							<br>U
							<br>PRE
						</code></p>

						<p>If you have any questions about how to use this part of the site, I'm more then happy to help. Send an email to <strong>admin@sbess.ca</strong> or call/text me <strong>(905) 407 9729</strong></p>
						<div class="submitbox">
							<button class="button-primary">View Clubs Archives</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="post-body">
			<div id="post-body-content">
				<div id="ss">

					<?php foreach ($rows as $row) : 

						if ( $row->id == '-1' ){
						 echo "<div id='loner' style='display: none'>";
						 $end = true;
						} else {
							$end = false;
						}


						$text_feilds = array(
							array( $row->club_name, 'Club Name', 'name'),
							array( $row->club_email, 'Club email', 'email'),
							array( $row->website, 'Website URL', 'web'),
							array( $row->contact_name, 'Primary Contact Name', 'cname'),
							array( $row->contact_email, 'Prmary Contact Email', 'cemail'),
							array( $row->facebook, 'Full Facebook URL', 'fb'),
							array( $row->twitter, 'Full Twitter URL', 'twitter'),
							array( $row->g_plus, 'If you really want G+...', 'gplus'),
							array( $row->meeting_time, 'Regular Meeting Time', 'meeting')
						);

						$long_feilds = array(
							array( $row->description, 'Full Club Description', 'des'),
							array( $row->major_events, 'Short Summary of Events', 'major')
						);
	
						$co = $row->collapsed;
	
						
					?>

					<form action="<?php echo $action_dir; ?>" enctype="multipart/form-data" method="get">
						
						<input type="hidden" name="action" value="update" />
						<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
						<input type="hidden" name="abs" value="<?php echo ABSPATH; ?>" />
						<input type="hidden" name="collapsed" value="<?php echo $co; ?>" />
						<input type="hidden" name="accepting" value="<?php echo $row->accepting; ?>" />
						
						<div class="postbox">
							<h3 data-collapsed="<?php echo $co; ?>"><?php echo $row->club_name; ?>, Club ID: <?php echo $row->id; ?><span class="instructions">Click to <?php
								if ($co == 1) {
									echo 'Expand';
								} else {
									echo 'Collapse';
								}
							?>
							</span></h3>
							<div class="inside<?php if ($co == 1) echo ' collapsed'; ?>">
								<table class="acf_input widefat">

									<?php foreach ($text_feilds as $i): ?>
									<tr>
										<td><label for="<?php echo $i[2]; ?>"><?php echo $i[1] ?></label></td>
										<td><input type="text" id="<?php echo $i[2]; ?>" name="<?php echo $i[2]; ?>" class="full-width" value="<?php echo $i[0]; ?>" /></td>
									</tr>
									<?php endforeach; ?>

									<tr>
										<td><label for="logo">Club Logo</label></td>
										<td>
											<input type="file" id="logo" name="logo"/>
											<img src="<?php echo $img_dir . '/' . $row->image ?>" alt="Club Logo">
										</td>
									</tr>

									<tr>
										<td><label for="date">Date Founded</label></td>
										<td>
											<select name="month" data-current="<?php echo $row->month_founded; ?>">
												<option value="1">January</option>
												<option value="2">February</option>
												<option value="3">March</option>
												<option value="4">April</option>
												<option value="5">May</option>
												<option value="6">June</option>
												<option value="7">July</option>
												<option value="8">August</option>
												<option value="9">Spetember</option>
												<option value="10">October</option>
												<option value="11">November</option>
												<option value="12">December</option>
											</select>
											<input type="text" name="year" value="<?php echo $row->year_founded; ?>" placeholder="year">
										</td>
									</tr>

									<?php foreach ($long_feilds as $i): ?>
									<tr>
										<td><label for="<?php echo $i[2]; ?>"><?php echo $i[1]; ?></label></td>
										<td><textarea type="text" id="<?php echo $i[2]; ?>" name="<?php echo $i[2]; ?>" class="full-width" style="width:100%" rows="4"><?php echo $i[0]; ?></textarea></td>
									</tr>
									<?php endforeach; ?>
								</table>
								<div class="submitbox">
									<a href="#" class="btn btn-info update"><i class="icon-check icon-white"></i> Update</button>
									<a href="#" class="btn btn-danger deletion"><i class="icon-trash icon-white"></i> Move to Archive</a>
								</div>
							</div>
						</div>
					</form>
					<?php
					
					if ($end == true) echo "</div>";
					endforeach; 
				
					?>

					<div class="addbox">
						<input type="button" value="Add New Block" class="add-block" />
					</div>
					
				</div
			</div>
		</div>
		<br class="clear" />
	</div>
</div>

<div class="modal fade" id="modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">�</button>
		<h3>Modal header</h3>
	</div>
	<div class="modal-body">
		<p>One fine body?</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
	</div>
</div>