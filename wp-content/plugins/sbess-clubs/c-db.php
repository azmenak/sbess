<?php

class clubsdb {

	function table_name () {
		global $wpdb;
		$ret = $wpdb->prefix."sbess_clubs";
		return $ret;
	}

	function update_dir () {
		$ret = plugins_url( 'c-update.php', __FILE__ );
		return $ret;
	}

	function delete_dir () {
		$ret = plugins_url( 'c-delete.php', __FILE__ );
		return $ret;
	}

	function uploads_dir () {
		$uploads = wp_upload_dir();
		$ret = $uploads['baseurl'] . '/clubs';
		return $ret;
	}

	function img_dir () {
		$uploads = wp_upload_dir();
		$ret = $uploads['baseurl'] . '/clubs/images';
		return $ret;
	}

	function action () {
		$ret = plugins_url( 'c-action.php', __FILE__ );
		return $ret;
	}

}

?>