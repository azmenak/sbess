# SBESS Clubs List Update and Display

The plugin was deisgned to be used as a shortcode on the clubs page -> [clubs_list], currenlty the database holds the following information:

- image
- club_name
- club_email
- website
- description
- contact_name
- contact_email
- facebook ( a URL to their facebook page )
- twitter ( a URL to their twitter page )
- g_plus ( for all the presidents that work at google )
- year_founded
- major-events ( long text area )
- meeting-time ( the regular meeting time for the club )
- accepting ( accepting new memebers currently 0 => not indicated, 1 => YES, 2 => NO )

- date_added
- date_mod

The access to this plugin is meant to be restricted to only those who need to update it.

## Roadmap

1. Turn description and major_events into WYSIWYG editors
2. Link to the calandar
3. Implement [clubs_short_list] which echos out a list of club name in
	<ul>
		<li>...</li>
	</ul>
4. Implement [clubs_short_list_linked] which adds <a> tags around the <li>s

