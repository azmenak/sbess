<?php

/**
 * Markdown Filter for {@link http://haml-lang.com/ Haml} class.
 * Parses the text with Markdown.
 * 
 * This is the extention class and the init() method
 * implemented to provide the vendorPath if the vendor class is not imported
 * elsewhere in the application (e.g. by a framework) and vendorClass if the
 * default class name is not correct.
 * @package			PHamlP
 * @subpackage	Haml.filters
 */

// Change this to the required path
define('VENDOR_PATH', 'markdown.php');
define('VENDOR_CLASS', 'Markdown_Parser');

class HamlMarkdownFilter extends _HamlMarkdownFilter {
	/**
	 * Initialise the filter with the $vendorPath
	 */
	public function init() {
		$this->vendorClass = VENDOR_CLASS;
		$this->vendorPath = VENDOR_PATH;		
		parent::init();
	}
}

?>